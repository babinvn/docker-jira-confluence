# Run Atlassian JIRA / Confluence using docker

This repo details some issues that I have encountered installing JIRA and
Confluence with various databases:

- MS SQL Server
- MySQL
- Oracle
- PostgreSQL

The network setup is for testing only as it does not provide any HTTPS.

Please make sure that docker machine's `/var/docker/lib` folder contains the
following files:

- `nginx-combo.conf` for all setups
- `mysql-confluence.cnf` for MySQL setup
- `mysql-connector-java-8.0.22.jar` for MySQL setup
- `ojdbc8.jar` for Oracle setup

Oracle licensing policy requires that you download the JDBC drivers yourself.
