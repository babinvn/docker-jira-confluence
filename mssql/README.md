# Atlassian JIRA / Confluence in docker with MS SQL Server database

For details on using MS SQL Serevr with docker please refer to
https://bitbucket.org/babinvn/docker-mssql

To create a self-contained JIRA database:

```
CREATE DATABASE [jiradb]
CONTAINMENT = PARTIAL
ON PRIMARY
    (NAME = N'jiradb', FILENAME = N'/var/opt/sqlserver/jiradb.mdf')
LOG ON
    (NAME = N'jiradb_log', FILENAME = N'/var/opt/sqlserver/jiradb_log.ldf');
GO
ALTER DATABASE [jiradb] COLLATE Latin1_General_CI_AI ;
GO
ALTER DATABASE [jiradb] SET READ_COMMITTED_SNAPSHOT ON WITH ROLLBACK IMMEDIATE;
GO
SELECT is_read_committed_snapshot_on FROM sys.databases WHERE name='jiradb';
GO
USE [jiradb];
GO
CREATE USER jira_user WITH PASSWORD = 'password';
GO
EXEC sp_addrolemember N'db_owner', N'jira_user';
GO
CREATE SCHEMA jira;
GO
```

For Atlassian Confluence this would be a little different:

```
CREATE DATABASE [wikidb]
CONTAINMENT = PARTIAL
ON PRIMARY
    (NAME = N'wikidb', FILENAME = N'/var/opt/sqlserver/wikidb.mdf')
LOG ON
    (NAME = N'wikidb_log', FILENAME = N'/var/opt/sqlserver/wikidb_log.ldf');
GO
ALTER DATABASE [wikidb] COLLATE SQL_Latin1_General_CP1_CS_AS;
GO
ALTER DATABASE [wikidb] SET READ_COMMITTED_SNAPSHOT ON WITH ROLLBACK IMMEDIATE;
GO
SELECT is_read_committed_snapshot_on FROM sys.databases WHERE name='wikidb';
GO
USE [wikidb];
GO
CREATE USER wiki_user WITH PASSWORD = 'password';
GO
EXEC sp_addrolemember N'db_owner', N'wiki_user';
GO
```

The following query in the scripts above should return the value of 1:

```
SELECT is_read_committed_snapshot_on FROM sys.databases WHERE name='<dbname>';
```
