# Nginx reverse-proxy for JIRA / Confluence

The proxy setup relies upon docker DNS server in resolver directive:

```
resolver                    127.0.0.11 valid=30s;
```

This ensures that Nginx will not fail to start if either `jira` or `wiki`
container is not running.

HTTPS setup is quite similar and may use a letsencrypt certificate but this is
not described here.
