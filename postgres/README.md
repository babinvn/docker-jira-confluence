# Atlassian JIRA / Confluence in docker with PostgreSQL database

This is by far the most trivial setup. It works fine on a 8GB docker machine.

To create JIRA database:

```
docker exec -u postgres -it postgres psql

create user jira_user with password '<password>';
create database jira with encoding 'UTF-8' LC_COLLATE 'en_US.utf8' LC_CTYPE 'en_US.utf8' owner jira_user;
```

To create Confluence database:

```
docker exec -u postgres -it postgres psql

create user wiki_user with password '<password>';
create database wiki with encoding 'UTF-8' LC_COLLATE 'en_US.utf8' LC_CTYPE 'en_US.utf8' owner wiki_user;
```
