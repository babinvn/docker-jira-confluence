# Atlassian JIRA / Confluence in docker with MYSQL database

This setup uses MariaDB for mysql docker image.

To create JIRA database:

```
docker exec -u root -it mysql mysql -u root -p

CREATE DATABASE jiradb CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
GRANT ALL PRIVILEGES ON `jiradb`.* TO 'jira_user'@'%' IDENTIFIED BY '<password>';
flush privileges;
```

For Confluence:

```
docker exec -u root -it mysql mysql -u root -p

CREATE DATABASE wikidb CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
GRANT ALL PRIVILEGES ON `wikidb`.* TO 'wiki_user'@'%' IDENTIFIED BY '<password>';
flush privileges;
```
