# Atlassian JIRA / Confluence in docker with Oracle database

This setup uses external Oracle database for JIRA and Confluence. Do NOT use
Oracle database on the same server with JIRA and/or Confluence. For details
on creating an Oracle docker image please refer to https://bitbucket.org/babinvn/docker-oracle

To create JIRA database:

```
docker exec -u oracle -it oracle sqlplus system@//127.0.0.1:1521/jiradb

create user jira_user identified by "<password>" default tablespace USERS quota unlimited on USERS;
grant connect to jira_user;
grant create table to jira_user;
grant create sequence to jira_user;
grant create trigger to jira_user;
```

To create Confluence database:

```
docker exec -u oracle -it oracle sqlplus system@//127.0.0.1:1521/jiradb

create user wiki_user identified by "<password>" default tablespace USERS quota unlimited on USERS;
grant connect to wiki_user;
grant resource to wiki_user;
grant create table to wiki_user;
grant create sequence to wiki_user;
grant create trigger to wiki_user;
```

Quite unexpectedly Confluence requires access to Oracle's RECYCLEBIN:

```
docker exec -u oracle -it oracle sqlplus sys@//127.0.0.1:1521/jiradb as sysdba

GRANT SELECT ON SYS.DBA_RECYCLEBIN TO WIKI_USER;
```

Confluence requires `ojdbc8`, so download and copy the file to `/var/docker/lib`
folder. For details please refer to https://confluence.atlassian.com/doc/database-setup-for-oracle-173821.html

JIRA will not start with default `dbconfig.xml`. For details please refer to
https://confluence.atlassian.com/adminjiraserver/connecting-jira-applications-to-oracle-938846856.html
Patch the `dbconfig.xml` file (or edit the file manually) then restart JIRA.
